# Asphodel Python Library

## About
The Asphodel Python Library wraps the Asphodel C Library, which supports communicating with USB and TCP devices communicating through the Asphodel communication protocol.

The Asphodel communication protocol was developed by Suprock Technologies (http://www.suprocktech.com)

## License
The Apshodel Python Library is licensed under the ISC license.

The ISC license is a streamlined version of the BSD license, and permits usage in both open source and propretary projects.
